# SENTIMENT API

## Software Specifications

- Ruby 2.7.2
- Rails 6.0.3
- PostgreSQL

## Installation

#### Set Ruby/RoR versions

It is necessary to have installed RVM for this example.

```bash

rvm install 2.7.2

```

```bash

rvm use ruby-2.7.2@rails6.0.3 --create

```

```bash

gem install rails -v 6.0.3

```

Check ruby/rails versions.

```bash

ruby -v

rails -v

```
#### Clone the repository

It's necessary to have installed Git for this example.

```bash

git clone git@bitbucket.org:xxx/xxx-xxxx-xxx-xxx.git

```

Then.

```bash

cd sentiment_api

```

#### Run the application

Run rails migrations.

```bash
rails db:create && rails db:migrate
```

Install rails dependencies.

```bash
bundle install
```

Run rails server.

```bash
rails s
```

## Resources

- [Access Points Documentation](https://documenter.getpostman.com/view/2691667/TVRn57T8)
- [sentiment-all Repository](https://github.com/armando1339/sentiment-all)
- [sentiment_tracker Repository](https://github.com/armando1339/sentiment_tracker)
- [Heroku Demo](https://git.heroku.com/polar-forest-68209.git)

## Be happy!
