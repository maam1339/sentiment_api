json.user do
  json.call(
    @user,
    :id,
    :email,
    :authentication_token,
    :created_at,
    :updated_at
  )
end