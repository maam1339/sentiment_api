TextsController.class_eval do

  # => Load needed methods and callbacks
  include Authentication

  # => Authenticate
  before_action :authenticate_user!

  # => Prevent Cross-Site Request Forgery (CSRF)
  protect_from_forgery with: :null_session
end