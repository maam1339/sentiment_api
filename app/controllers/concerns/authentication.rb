module Authentication
  extend ActiveSupport::Concern

  # => Callback that allow for controllers to handle authentication
  # token through devise methods in the context of a model pased.
  #
  included do
    acts_as_token_authentication_handler_for User, fallback: :none
  end

  # => Devise method that it is overwritten to generate the current_*
  # according to the "single table inherit" patern of rails.
  # Use the type parameter to generate the current_*
  #
  def authenticate_user!
    self.class.send(:attr_reader, :current_user)
    instance_variable_set(:@current_user, super)
  end
end