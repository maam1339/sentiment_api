module V1
  module Users
    class SessionsController < ApplicationController
      before_action :authenticate_user!, only: %i(destroy)
      before_action(
        :set_user,
        :user_found,
        :user_valid_password,
        only: %i(create)
      )

      def create
        render :show, status: :created
      end

      def destroy
        current_user&.authentication_token = nil
        current_user.save

        render(
          json: { message: I18n.t('devise.sessions.signed_out') },
          status: :ok
        )
      end

      private

      # => *
      def set_user
        @user = User.find_by email: user_params.fetch(:email)
      end

      # => *
      def user_params
        params.require(:user).permit(:email, :password)
      end

      # => *
      def user_found
        unless @user
          return render(
            json: { error: I18n.t('devise.failure.invalid') },
            status: :not_found
          )
        end
      end

      # => *
      def user_valid_password
        unless @user.valid_password? params.fetch(:user).fetch(:password)

          return render(
            json: { error: I18n.t('devise.failure.invalid') },
            status: :unauthorized
          )
        end
      end
    end
  end
end