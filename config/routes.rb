Rails.application.routes.draw do

  # => Routes configurations for sentiment_tracker
  sentiment_tracker

  # => skipping session controller from devise 
  devise_for :users, defaults: { format: :json }, skip: [:session]

  # => V1 of the API
  namespace :v1, defaults: { format: :json } do

    # => Users authentication
    namespace :users do
      resource :sessions, only: %i(create destroy)
    end
  end
end
