source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.4'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # => rspec-rails is a testing framework for Rails 3+
  gem 'rspec-rails', '~> 4.0', '>= 4.0.1'
  # => Test common for Rails functionality
  gem 'shoulda-matchers', '~> 4.2'
  # => Making callback tests easy on the fingers and eyes
  gem 'shoulda-callback-matchers', '~> 1.1', '>= 1.1.4'
  # => fixtures replacement for test
  gem 'factory_bot_rails', '~> 5.1', '>= 5.1.1'
  # => FAker is used to easily generate fake data: names, addresses, phone numbers, etc.
  gem 'faker', '~> 2.10', '>= 2.10.2'
  # => test functionality
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.4'
  # => terminal and stop routine time line of applications rails
  gem 'pry-rails', '~> 0.3.9'
end

group :development do
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # => When mail is sent from your application, Letter Opener will open a preview in the browser instead of sending.
  gem 'letter_opener', '~> 1.7'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# => Plugin Rails for keep track of sentiment analysis process.
gem 'sentiment_tracker', '~> 1.0.2beta'
# => Flexible authentication solution for Rails with Warden
gem 'devise', '~> 4.7', '>= 4.7.3'
# => Simple (but safe) token authentication for Rails apps or API with Devise.
gem 'simple_token_authentication', '~> 1.17'
